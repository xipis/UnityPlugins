﻿using UnityEngine;

public class NativeAndroid
{
    public void GetLaunchInfo(string objName, string methodName)
    {
#if UNITY_ANDROID
        using (AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                object[] args = new object[2];
                args[0] = objName;
                args[1] = methodName;
                jo.Call("GetLaunchInfo", args);
            }
        }
#endif
    }

    public void PhotoInsert(string imgName)
    {
#if UNITY_ANDROID
        using (AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                jo.Call("PhotoInsert", imgName);
            }
        }
#endif
    }

    public void PhotoTake(string objName, string methodName)
    {
#if UNITY_ANDROID
        using (AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                object[] args = new object[2];
                args[0] = objName;
                args[1] = methodName;
                jo.Call("PhotoTake", args);
            }
        }
#endif
    }

    public void PhotoPick(string objName, string methodName)
    {
#if UNITY_ANDROID
        using (AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                object[] args = new object[2];
                args[0] = objName;
                args[1] = methodName;
                jo.Call("PhotoPick", args);
            }
        }
#endif
    }

    /// <summary>
    /// 微信api初始化
    /// </summary>
    /// <param name="appId"></param>
    /// <param name="appSecret"></param>
    public void WxInit(string appId)
    {
#if UNITY_ANDROID
        using (AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                jo.Call("WxInit", appId);
            }
        }
#endif
    }

    /// <summary>
    /// 微信授权登录
    /// </summary>
    public void WxLogin(string objName, string methodName)
    {
#if UNITY_ANDROID
        using (AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                object[] args = new object[2];
                args[0] = objName;
                args[1] = methodName;
                jo.Call("WxLogin", args);
            }
        }
#endif
    }

    /// <summary>
    /// 微信分享文字 scene分享类型 好友==0 朋友圈==1
    /// </summary>
    public void WxShareText(string content, int scene)
    {
#if UNITY_ANDROID
        using (AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                object[] args = new object[2];
                args[0] = content;
                args[1] = scene;
                jo.Call("WxShareText", args);
            }
        }
#endif
    }

    /// <summary>
    /// 微信分享图片 scene分享类型 好友==0 朋友圈==1
    /// </summary>
    public void WxShareImg(string imgName, int scene)
    {
#if UNITY_ANDROID
        using (AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                object[] args = new object[2];
                args[0] = imgName;
                args[1] = scene;
                jo.Call("WxShareImg", args);
            }
        }
#endif
    }

    /// <summary>
    /// 微信分享链接 scene分享类型 好友==0 朋友圈==1
    /// </summary>
    public void WxShareUrl(string url, string title, string description, string imgName, int scene)
    {
#if UNITY_ANDROID
        using (AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                object[] args = new object[5];
                args[0] = url;
                args[1] = title;
                args[2] = description;
                args[3] = imgName;
                args[4] = scene;
                jo.Call("WxShareUrl", args);
            }
        }
#endif
    }
}