﻿using UnityEngine;
using System.IO;
using UnityEditor.Callbacks;
using UnityEditor;
using System.Collections;

#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif

public class UnityPluginsProcessBuild
{
#if UNITY_IOS
    [PostProcessBuildAttribute(88)]
    public static void onPostProcessBuild(BuildTarget buildTarget, string path)
    {
        if (buildTarget != BuildTarget.iOS)
        {
            Debug.LogWarning("Target is not iPhone. XCodePostProcess will not run");
            return;
        }
        //导入文件
        PBXProject proj = new PBXProject();
        string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
        proj.ReadFromFile(projPath);
        //xcode Target
        string target = proj.TargetGuidByName("Unity-iPhone");

        // add extra framework(s)
        proj.AddFrameworkToProject(target, "AssetsLibrary.framework", false);
        proj.AddFrameworkToProject(target, "Photos.framework", true);
        proj.AddFrameworkToProject(target, "SystemConfiguration.framework", true);
        proj.AddFrameworkToProject(target, "security.framework", false);
        proj.AddFrameworkToProject(target, "CoreTelephony.framework", false);

        string fileGuidLibc = proj.AddFile("usr/lib/libc++.tbd", "Libraries/libc++.tbd", PBXSourceTree.Sdk);
        proj.AddFileToBuild(target, fileGuidLibc);
        string fileGuidLibz = proj.AddFile("usr/lib/libz.tbd", "Libraries/libz.tbd", PBXSourceTree.Sdk);
        proj.AddFileToBuild(target, fileGuidLibz);
        string fileGuidSqlite = proj.AddFile("usr/lib/libsqlite3.tbd", "Libraries/libsqlite3.tbd", PBXSourceTree.Sdk);
        proj.AddFileToBuild(target, fileGuidSqlite);

        proj.AddBuildProperty(target, "OTHER_LDFLAGS", "-ObjC");

        // rewrite to file
        File.WriteAllText(projPath, proj.WriteToString());

        string plistPath = path + "/Info.plist";
        PlistDocument plist = new PlistDocument();
        plist.ReadFromString(File.ReadAllText(plistPath));

        // Get root
        PlistElementDict plistElementDict = plist.root;

        plistElementDict.SetString("NSPhotoLibraryAddUsageDescription", "$(PRODUCT_NAME)插入相册");
        plistElementDict.SetString("NSPhotoLibraryUsageDescription", "$(PRODUCT_NAME)访问相册");

        PlistElementArray queriesSchemes = plistElementDict.CreateArray("LSApplicationQueriesSchemes");
        ArrayList queries = new ArrayList();
        queries.Add("weixin");
        queries.Add("wechat");
        foreach (string query in queries)
        {
            queriesSchemes.AddString(query);
        }

        PlistElementArray plistElementArray = plistElementDict.CreateArray("CFBundleURLTypes");
        PlistElementDict dict1 = plistElementArray.AddDict();
        dict1.SetString("CFBundleURLName", "weixin");
        PlistElementArray urlArray1 = dict1.CreateArray("CFBundleURLSchemes");
        ArrayList schemes1 = new ArrayList();
        //微信的appid
        schemes1.Add("wx80819d29219b428f");
        foreach (string schemeStr in schemes1)
        {
            urlArray1.AddString(schemeStr);
        }

        // Write to file
        File.WriteAllText(plistPath, plist.WriteToString());
    }
#endif
}